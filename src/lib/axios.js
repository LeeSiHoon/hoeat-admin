import axios from 'axios'

const _axios = axios.create({
  baseURL: 'http://admin.hoeat.co.kr'
})

_axios.defaults.headers.common['X-User-Agent'] = `hoeat-order-manager/2.0.0`

export default _axios
export const setBaseURL = function (url) {
  _axios.defaults.baseURL = url
}
