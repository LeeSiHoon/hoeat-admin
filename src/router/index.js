// eslint-disable-next-line import/no-webpack-loader-syntax
import 'expose-loader?$!expose-loader?jQuery!jquery'
import $ from 'jquery'
import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/routes/Dashboard'
import Brand from '@/components/routes/Brand'
import Status from '@/components/routes/Status'
// Brand 관련 Router 설정
import BrandMenu from '@/components/routes/brand/Menu'
import BrandDetail from '@/components/routes/brand/Detail'
import BrandJoin from '@/components/routes/brand/Join'
import BrandCategory from '@/components/routes/brand/Category'
import Toasted from 'vue-toasted'

Vue.use(Router)
Vue.use(Toasted)
window.jquery = $
window.$ = $

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'Login',
      meta: {title: '로그인'}
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: {title: '대시보드'}
    },
    {
      path: '/brand',
      name: 'Brand',
      component: Brand,
      meta: {title: '브랜드'}
    },
    {
      path: '/status',
      name: 'Status',
      component: Status
    },
    {
      path: '/brand/menu',
      name: 'BrandMenu',
      component: BrandMenu,
      meta: {title: '브랜드 메뉴'}
    },
    {
      path: '/brand/detail',
      name: 'BrandDetail',
      component: BrandDetail,
      meta: {title: '브랜드 상세'}
    },
    {
      path: '/brand/join',
      name: 'BrandJoin',
      component: BrandJoin,
      meta: {title: '브랜드 가입'}
    },
    {
      path: '/brand/category',
      name: 'BrandCategory',
      component: BrandCategory,
      meta: {title: '브랜드 카테고리'}
    },
    {
      path: '*',
      redirect: '/dashboard'
    }
  ]
})
